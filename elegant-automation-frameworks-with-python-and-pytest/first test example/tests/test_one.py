'''2022-05-22 13:34:41'''
from pytest import mark


@mark.two
def test_one():
    assert True


@mark.one
def test_one_marked():
    assert True


@mark.one
@mark.webdvr
def test_apple_website(chrome_browser):
    chrome_browser.get('https://www.apple.com/mx/')
    assert True


@mark.one
@mark.webdvr
def test_google_website(chrome_browser):
    chrome_browser.get('https://www.google.com/mx/')
    assert True
