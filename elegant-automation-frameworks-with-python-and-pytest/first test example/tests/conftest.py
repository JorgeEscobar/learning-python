'''2022-05-22 14:07:28
This is a fixture for pytest.
A fixture is code that can be taken from other tests files and reused.
This is useful to set up your tests and not writing setup code all over again.'''
from pytest import fixture
from selenium import webdriver
from config import Config


@fixture(scope='session')
def chrome_browser():
    '''in scope=fuction you may also use "session", the difference is that:
        In scope="function", the fixture runs every time before and after every test function.
        In scope="session", the fixture runs every time before and after once for the entire test suit..
    '''
    yield webdriver.Chrome('./chromedriver')
    print('Teardown!')


def pytest_addoption(parser):
    parser.addoption(
        "--env",
        action="store",
        help="Environment to run tests against"
    )


@fixture(scope='session')
def env(request):
    return request.config.getoption("--env")


@fixture(scope='session')
def app_config(env):
    cfg = Config(env)
    return cfg
