import logging
import math

# Create and condigure logger.
LOG_FORMAT = '%(levelname)s %(asctime)s - %(message)s'
logging.basicConfig(filename='./errors.log', level=logging.DEBUG, format=LOG_FORMAT)

logger = logging.getLogger()

# Test logger.
# logger.info("Our first message.")

# print(logger.level)

def quadratic_formula(a, b, c):
    '''Return solutions to the equation ax^2 + bx + c = 0.'''
    logger.info("quadradic_formula({0}, {1}, {2}".format(a, b, c))

    # Compute discriminant
    logger.debug('# Compute discriminant')
    disc = b**2 - 4*a*c

    # Compute the two roots
    logger.debug('# Compute the two roots')
    root1 = (-b + math.sqrt(disc)) / (2*a)
    root2 = (-b - math.sqrt(disc)) / (2*a)

    # Return Roots
    logger.debug('# Return Roots')
    return (root1, root2)

roots = quadratic_formula(1, 0, 1)
print(roots)