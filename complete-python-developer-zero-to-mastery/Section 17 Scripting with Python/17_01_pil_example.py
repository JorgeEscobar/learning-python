'''2022-05-16 11:41:35'''
from PIL import Image, ImageFilter
from matplotlib.image import thumbnail

img = Image.open('./img/raw/Pikachu.jpeg')

# Blur filter
blur_img = img.filter(ImageFilter.BLUR)
blur_img.save('./img/processed/blur.png', 'png')

# Grayed out
gray_img = img.convert('L')
gray_img.save('./img/processed/grayed.png', 'png')

# Rotate 90º
rotate_img = img.rotate(90)
rotate_img.save('./img/processed/rotate.png', 'png')

# Rotate 90º
resize_img = img.resize((300, 300))
resize_img.save('./img/processed/resize.png', 'png')

# Region crop
box = (100, 100, 400, 400)
region = img.crop(box)
region.save('./img/processed/crop.png', 'png')

# Thumbnail
thumbnail_img = img
thumbnail_img.thumbnail((200, 200))
thumbnail_img.save('./img/processed/thumbnail.png', 'png')

# Show image
thumbnail_img.show()
