'''2022-05-16 22:10:37'''
# !/usr/bin/python
# Adding a watermark to a single-page PDF

import PyPDF2

input_file = "super.pdf"
output_file = "example-drafted.pdf"
watermark_file = "wtr.pdf"

with open(input_file, "rb") as filehandle_input:
    # read content of the original file
    pdf = PyPDF2.PdfFileReader(filehandle_input)

    with open(watermark_file, "rb") as filehandle_watermark:
        # read content of the watermark
        watermark = PyPDF2.PdfFileReader(filehandle_watermark)

        # get first page of the watermark PDF
        first_page_watermark = watermark.getPage(0)

        # create a pdf writer object for the output file
        pdf_writer = PyPDF2.PdfFileWriter()

        for page_number in range(pdf.getNumPages()):
            # get first page of the original PDF
            page = pdf.getPage(page_number)

            # merge the two pages
            page.mergePage(first_page_watermark)

            # add page
            pdf_writer.addPage(page)

        with open(output_file, "wb") as filehandle_output:
            # write the watermarked file to the new file
            pdf_writer.write(filehandle_output)
