# You will need to PIP INSTALL tweepy for this to work and also create a twitter API. Run this on your own machine, not in this Repl.
import tweepy
import time
import pyjokes
from dotenv import load_dotenv
import os

# Load credentials for Twitter from 'Section 17 Scripting with Python/venv/.env'
load_dotenv()

# Reads variables stored with:
# os.environ.get('variable_name')
consumer_key = os.environ.get('consumer_key')
consumer_secret = os.environ.get('consumer_secret')
access_token = os.environ.get('access_token')
access_token_secret = os.environ.get('access_token_secret')


auth = tweepy.OAuth1UserHandler(
    consumer_key, consumer_secret, access_token, access_token_secret
)

api = tweepy.API(auth)

client = tweepy.Client(
    consumer_key=consumer_key, consumer_secret=consumer_secret,
    access_token=access_token, access_token_secret=access_token_secret
)


def get_public_tweets():
    public_tweets = api.home_timeline()
    for tweet in public_tweets:
        print(tweet.text)


def limit_handle(cursor):
    while True:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            time.sleep(300)


# Get the User object for twitter...
def get_user(requested_screen_name):
    user = api.get_user(screen_name=requested_screen_name)
    return user


def get_user_friends(user):
    for friend in limit_handle(user.friends()):
        yield f'@{friend.screen_name}'
        print(f'@{friend.screen_name}')


def get_twitter_followers(user):
    print(f'Target Twitter user: @{user.screen_name}')
    print(f'Current followers: {user.followers_count:,}')
    for friend in limit_handle(user.friends()):
        print(f'@{friend.screen_name}')
    return 0

# Create Tweet

# The app and the corresponding credentials must have the Write permission

# Check the App permissions section of the Settings tab of your app, under the
# Twitter Developer Portal Projects & Apps page at
# https://developer.twitter.com/en/portal/projects-and-apps

# Make sure to reauthorize your app / regenerate your access token and secret
# after setting the Write permission


response = client.create_tweet(
    text=pyjokes.get_joke()
)
print(f"https://twitter.com/user/status/{response.data['id']}")

# user = get_user('eddiejaoude')
# get_twitter_followers(user)

""" 

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

user = api.me()
print(user.name)  # prints your name.
print(user.screen_name)
print(user.followers_count)

search = "zerotomastery"
numberOfTweets = 2



 """

# Be nice to your followers. Follow everyone!
""" for follower in limit_handle(tweepy.Cursor(api.followers).items()):
    if follower.name == 'Usernamehere':
        print(follower.name)
        follower.follow() """


# Be a narcisist and love your own tweets. or retweet anything with a keyword!
""" for tweet in tweepy.Cursor(api.search, search).items(numberOfTweets):
    try:
        tweet.favorite()
        print('Retweeted the tweet')
    except tweepy.TweepError as e:
        print(e.reason)
    except StopIteration:
        break """
