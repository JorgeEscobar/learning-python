import tweepy
import pyjokes
from dotenv import load_dotenv
import os

# Load credentials for Twitter from 'Section 17 Scripting with Python/venv/.env'
load_dotenv()

# Reads variables stored with:
# os.environ.get('variable_name')
consumer_key = os.environ.get('consumer_key')
consumer_secret = os.environ.get('consumer_secret')
access_token = os.environ.get('access_token')
access_token_secret = os.environ.get('access_token_secret')

client = tweepy.Client(
    consumer_key=consumer_key, consumer_secret=consumer_secret,
    access_token=access_token, access_token_secret=access_token_secret
)

# Create Tweet

# The app and the corresponding credentials must have the Write permission

# Check the App permissions section of the Settings tab of your app, under the
# Twitter Developer Portal Projects & Apps page at
# https://developer.twitter.com/en/portal/projects-and-apps

# Make sure to reauthorize your app / regenerate your access token and secret
# after setting the Write permission

response = client.create_tweet(
    text=f'This is a #Python 🐍 joke:\n\n{pyjokes.get_joke()}'
)
print(f"https://twitter.com/user/status/{response.data['id']}")
