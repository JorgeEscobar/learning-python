import sys
from pathlib import Path
from PIL import Image


try:
    # grab first and second argument
    first_argument = sys.argv[1]
    second_argument = sys.argv[2]

    # Check if second argument exists as a directory, if not create it
    destiny_path = Path(second_argument)
    destiny_path.mkdir(parents=True, exist_ok=True)

    source_path = Path(first_argument)
    if not source_path.exists():
        print('Source path does not exists.')
        exit(1)

    # Loop through pokedex images in ./img/raw
    for child in source_path.iterdir():
        if child.suffix == 'jpg' or 'jpeg':
            # convert images to PNG
            img = Image.open(child)
            converted_img = Path(destiny_path).joinpath(f'{child.stem}.png')

            # Save to the new folder
            img.save(converted_img, 'png')

except IndexError as err:
    print('You need to pass two arguments: img/source/directory/ img/destiny/directory')
    exit(1)
