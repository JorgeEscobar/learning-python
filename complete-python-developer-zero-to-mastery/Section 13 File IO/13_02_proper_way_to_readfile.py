'''2022-05-13 10:56:28'''

# Read only mode, chosen by default.
with open('hello.txt') as my_file:
    print(my_file.readlines())

# Write mode.
with open('hello.txt', mode='w') as my_file:
    print(my_file.write('This was written from a python script.'))

# Read and Write mode.
with open('hello.txt', mode='r+') as my_file:
    print(my_file.read())

# Append mode.
with open('hello.txt', mode='a') as my_file:
    print(my_file.write('This was appened.'))
