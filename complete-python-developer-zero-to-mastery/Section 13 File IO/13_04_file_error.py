'''2022-05-13 11:45:05'''
try:
    with open('hello.txt', mode='x') as my_file:
        print(my_file.read())
except FileNotFoundError as err:
    print('The file does not exists.')
    raise err
except IOError as err:
    print('IO ERROR')
    raise err
