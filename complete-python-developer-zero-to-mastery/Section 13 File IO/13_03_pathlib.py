'''2022-05-13 11:35:49
pathlib is a built in library that allows to use path to file systems on both Windows and Unix type file systems.
'''
from pathlib import Path

p = Path('.')
print([x for x in p.iterdir() if x.is_dir()])
