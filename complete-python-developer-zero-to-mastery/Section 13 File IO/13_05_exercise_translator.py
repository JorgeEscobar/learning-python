'''2022-05-13 12:00:49'''
from translate import Translator
import pyjokes

translator = Translator(to_lang="ja")
en_jokes = []
jokes = pyjokes.get_jokes('en', 'neutral')
try:
    with open('pyjokes.txt', mode='w') as my_file:
        for joke in jokes:
            my_file.write(joke + '\n')
except FileNotFoundError as err:
    print('The file does not exists.')
    raise err
except IOError as err:
    print('IO ERROR')
    raise err

# Translate the jokes to Japanese.
try:
    with open('pyjokes.txt', mode='r') as en_jokes_file:
        en_jokes = en_jokes_file.readlines()

    with open('ja_pyjokes.txt', mode='w') as my_file:
        for joke in en_jokes:
            translation = translator.translate(joke + '\n')
            # my_file.write(translation)
            print(translation)
except FileNotFoundError as err:
    print('The file does not exists.')
    raise err
except IOError as err:
    print('IO ERROR')
    raise err
