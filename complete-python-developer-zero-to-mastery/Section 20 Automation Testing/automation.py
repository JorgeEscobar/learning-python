'''2022-05-19 11:53:05'''
from selenium import webdriver

chrome_browser = webdriver.Chrome('./chromedriver')
chrome_browser.get('https://demo.seleniumeasy.com/basic-first-form-demo.html')
chrome_browser.maximize_window()

# Select button text
show_message_button = chrome_browser.find_element_by_css_selector(
    '#get-input > .btn')

assert 'Selenium Easy Demo - Simple Form to Automate using Selenium' in chrome_browser.title

# Select text input field
user_message = chrome_browser.find_element_by_id('user-message')
user_message.clear()
user_message.send_keys('Im super cool!')
show_message_button.click()

# Quit all sessions opened with this script
chrome_browser.quit()
