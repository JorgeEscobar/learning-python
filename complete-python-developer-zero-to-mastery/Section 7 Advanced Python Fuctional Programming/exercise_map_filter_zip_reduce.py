from functools import reduce

from sympy import true


def to_upper(item):
    '''2022-05-08 13:09:04'''
    return item.upper()


def filter_percent(item):
    '''2022-05-08 13:10:02'''
    return item > 50


def reduce_add(accumulator, item):
    '''2022-05-08 13:20:54'''
    return accumulator + item

    # 1 Capitalize all of the pet names and print the list
my_pets = ['sisi', 'bibi', 'titi', 'carla']

print(list(map(to_upper, my_pets)))

# 2 Zip the 2 lists into a list of tuples,
# but sort the numbers from lowest to highest.
my_strings = ['a', 'b', 'c', 'd', 'e']
my_numbers = [5, 4, 3, 2, 1]
my_numbers.sort()

print(list(zip(my_numbers, my_strings)))

# 3 Filter the scores that pass over 50%
scores = [73, 20, 65, 19, 76, 100, 88, 50, 3, 4, 5, 51]
print(list(filter(filter_percent, scores)))

# 4 Combine all of the numbers that are in a list
# on this file using reduce (my_numbers and scores).
# What is the total?
print(my_numbers + scores)
print(reduce(reduce_add, (my_numbers + scores)))
