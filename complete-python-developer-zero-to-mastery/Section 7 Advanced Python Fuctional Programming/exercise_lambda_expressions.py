'''2022-05-08 13:42:16'''
from numpy import sort


my_list = [5, 4, 3]

print(list(map(lambda item: item**2, my_list)))

# List sorting
a = [(0, 2), (4, 3), (9, 9), (10, -1)]
a.sort(key=lambda item: item[1])
print(a)
