'''
2022-05-02 17:17:22
'''


class Math:
    '''
    2022-05-02 17:18:00
    '''

    def __init__(self, value) -> None:
        self.value = value

    @staticmethod
    def add(num1, num2):
        '''
        2022-05-02 17:18:34
        '''
        return num1 + num2

    @classmethod
    def make_math_obj(cls, number):
        '''
        2022-05-02 17:22:24
        '''
        return cls(number)


print(Math.add(0.03, 0.02))  # staticmethod

math1 = Math.make_math_obj(3)

print(f'The new Math object value is: {math1.value}')
