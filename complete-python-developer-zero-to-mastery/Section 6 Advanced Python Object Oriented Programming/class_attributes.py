'''
2022-05-02 16:21:40
Class member attributes and how to use it with a bank account class.
'''


class BankAccount:
    '''
    2022-05-02 16:29:38
    '''
    interest_rate = 0.12
    dollar_to_mexican_peso_multiplier = 20.48

    def __init__(self, bank_account_number, balance) -> None:
        self.balance = balance
        self.bank_account_number = bank_account_number

    def get_as_dollars(self) -> float:
        '''
        Returns the balance as US dollars.
        '''
        return round(self.balance / BankAccount.dollar_to_mexican_peso_multiplier, 2)


account1 = BankAccount(123, 1000.00)
account2 = BankAccount(456, 2000.00)

print(f'{account1.bank_account_number}: MXN ${account1.balance} USD ${account1.get_as_dollars()}')
print(f'{account2.bank_account_number}: MXN ${account2.balance} USD ${account2.get_as_dollars()}')
