'''
2022-05-02 15:58:23
This is an example of a simple class, constructor and attributes with a basic method.
'''


class Player:
    '''
    Player class.
    '''

    def __init__(self, name):
        self.name = name
        self.health_points = 10
        self.ammo = 30
        self.is_alive = True

    def get_hit(self, damage):
        '''
        Returns the name of the Player
        '''
        self.health_points -= damage


player1 = Player('Cindy')
player2 = Player('John')

print(player1.name + ' HP: ' + str(player1.health_points))
print(player2.name + ' HP: ' + str(player2.health_points))

player1.get_hit(3)
player2.get_hit(1)
player2.health_points = 100

print(player1.name + ' HP: ' + str(player1.health_points))
print(player2.name + ' HP: ' + str(player2.health_points))
