'''
2022-05-02 16:45:41
'''
# Given the below class:


class Cat:
    '''
    2022-05-02 16:45:31
    '''
    species = 'mammal'

    def __init__(self, name, age):
        self.name = name
        self.age = age


# 1 Instantiate the Cat object with 3 cats
cat1 = Cat('Andres', 17)
cat2 = Cat('Missifu', 1)
cat3 = Cat('Michi', 3)
cat4 = Cat('Juri', 12)
cat5 = Cat('Mikala', 20)
cat6 = Cat('Coco', 4)
cat7 = Cat('Kira', 5)


# 2 Create a function that finds the oldest cat
def get_oldest_cat(list_of_cats) -> Cat:
    '''
    2022-05-02 16:57:58
    '''
    oldest_cat = Cat('', 0)
    for cat in list_of_cats:
        if oldest_cat.age < cat.age:
            oldest_cat = cat
    return oldest_cat


# 3 Print out: "The oldest cat is x years old.".
# x will be the oldest cat age by using the function in #2
list_of_cats = [cat1, cat2, cat3, cat4, cat5, cat6, cat7]
oldest = get_oldest_cat(list_of_cats)

print(
    f'The oldest cat name is {oldest.name}, and it\'s age is: {oldest.age}')
