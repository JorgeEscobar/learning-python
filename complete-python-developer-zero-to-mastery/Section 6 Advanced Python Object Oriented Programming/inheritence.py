'''2022-05-07 12:40:46'''


class Animal:
    '''2022-05-07 12:41:51'''
    total_animals = 0

    @classmethod
    def add_animal(cls) -> None:
        '''2022-05-07 13:04:16'''
        cls.total_animals += 1

    species = 'unknown'


class Cat(Animal):
    '''
    2022-05-02 16:45:31
    '''

    def __init__(self, name, age, species='mammal') -> None:
        self.species = species
        # super() returns a copy of the Animal class, not the class itself.
        Animal.add_animal()
        self.name = name
        self.age = age


cat1 = Cat('Michi', 2)
cat2 = Cat('Elyse', 5, 'Fungus')


list_of_cats = [cat1, cat2, Cat('Elyse', 5, 'Fungus'),
                Cat('Elyse', 5, 'Fungus'), Cat('Elyse', 5, 'Fungus')]
for cat in list_of_cats:
    print(f'I have a cat named {cat.name} and is {cat.age} years old.')
    print(f'{cat.name} is a {cat.species}.')

print(f'Total cats = {Cat.total_animals}')
print(f'Total animals = {Animal.total_animals}')
