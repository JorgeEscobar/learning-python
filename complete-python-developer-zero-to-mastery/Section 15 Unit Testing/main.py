'''2022-05-14 13:43:34'''


def do_stuff(num=0):
    try:
        if num:
            return int(num + 5)
        else:
            return 'Please enter a number.'
    except TypeError as err:
        return err
