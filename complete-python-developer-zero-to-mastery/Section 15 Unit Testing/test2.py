'''2022-05-14 13:44:32'''
import unittest
import main


class TestMain(unittest.TestCase):
    def test_do_stuff(self):
        '''Test4'''
        test_param = 10
        result = main.do_stuff(test_param)
        self.assertEqual(result, 15)

    def test_do_stuff2(self):
        '''Test5'''
        test_param = 'sdkfhjskfhsfdjk'
        result = main.do_stuff(test_param)
        self.assertIsInstance(result, TypeError)

    def test_do_stuff3(self):
        '''Test6'''
        test_param = None
        result = main.do_stuff(test_param)
        self.assertEqual(result, 'Please enter a number.')


if __name__ == '__main__':
    unittest.main()
