'''2022-05-12 12:55:53
In order to use this program successfully you need to run this on the command line, beforehand:

pip3 install pyjokes
'''
import pyjokes

joke = pyjokes.get_joke('en', 'neutral')

print(joke)
