'''2022-05-12 09:15:39'''
import sys
from random import randint
try:
    first = int(sys.argv[1])
    last = int(sys.argv[2])
except IndexError:
    print('You need to input 2 numbers.')
    exit()
except ValueError:
    print('The values entered need to be integer numbers.')
    exit()
# Game logic

solution = randint(first, last)

guess = -1
while solution != guess:
    guess = int(input('Give me a number: '))
    if solution > guess:
        print('HIGHER!')
    elif solution < guess:
        print('lower.')

print(f'You guess it! You Win! It\'s {solution}')
