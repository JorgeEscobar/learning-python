def sum(*args) -> int:
    sum = 0
    for num in args:
        sum += num
    return sum
