def sum(*args) -> int:
    sum = 0
    for num in args:
        sum += num
    return sum


def mul(*args) -> int:
    mul = 1
    for num in args:
        mul *= num
    return mul
