'''2022-05-08 20:16:03 decorator example'''


def my_decorator(function):
    '''2022-05-08 20:20:49'''
    def wraper_function():
        print('**************')
        function()
        print('**************')
    return wraper_function


@my_decorator
def say_hello():
    '''2022-05-08 20:20:57'''
    print('Hello.')


@my_decorator
def say_bye():
    '''2022-05-08 20:21:02'''
    print('Bye.')


say_hello()
say_bye()
