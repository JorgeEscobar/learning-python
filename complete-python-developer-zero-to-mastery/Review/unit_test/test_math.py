'''2022-05-20 14:35:17'''
# This imports the 'unittest' library
# and 'main' is the program to actually test.
import unittest
import main


class TestMain(unittest.TestCase):
    # def setUp(self):
    #     print('About to run a test.')

    # def tearDown(self) -> None:
    #     print('Tearingdown...')

    def test_sum_two_numbers(self):
        '''Test sum two numbers: 1 + 1 = 2'''
        test_param = 1
        expected_result = 2
        result = main.sum_numbers(test_param, test_param)
        self.assertEqual(result, expected_result)

    def test_sum_two_negative_numbers(self):
        '''Test sum two negative numbers: -1 + -1 = -2'''
        test_param = -1
        expected_result = -2
        result = main.sum_numbers(test_param, test_param)
        self.assertEqual(result, expected_result)

    def test_sum_numbers_with_text(self):
        '''Test sum numbers with text: 1 + '1' = Raise error'''
        test_param1 = 1
        test_param2 = '1'
        expected_result = 2
        result = 0
        with self.assertRaises(TypeError):
            result = main.sum_numbers(test_param1, test_param2)
        self.assertEqual(result, expected_result)


if __name__ == '__main__':
    unittest.main()
