'''2022-05-20 09:49:19
This is a playground to test out generator fuctions.
'''
import time
from generator_performance import performance


# Basic generator, this function yields a given number multipolied by 2.
def generator_function(num):
    for i in range(num):
        yield i*2


def tweeter_follower_generator(tweeter_followers):
    for follower in tweeter_followers:
        yield f'@{follower}'


# Generator for dictionaries
def dict_generator(dict):
    # You may yield two or more values, python creates a tuple, example: ('key1', 'Value1')
    for key in dict:
        yield key, dict[key]


# Generator for dictionaries
# You can use enumerate if you want indexes
# (remember that dictionaries don't have an order):
def dict_generator_with_indexes(dict):
    for index, key in enumerate(dict):
        yield index, key, dict[key]
        # time.sleep(1)


# Generator test function
@performance
def test_generator(generator):
    for generator_item in generator:
        print(f'{generator_item}')


# Main code
follower_list = ['JannikWempe', 'svpino', 'MyAspirants', 'AjeetKhamesra1', 'DevynJonash', 'KanikaTolver', 'lauraklein', 'buse_ozvatan', 'terrahanks', 'ykdojo',
                 'juliafmorgado', 'therajatkapoor', 'MotivateUTech', 'HelloKateOnline', 'kushlani_ds', 'Chamathka_0815', 'NishamaniF', 'EmDeeS11', 'j_uditha', 'Ashishkagn']
example_dict = {
    'key1': 'Value1',
    'key2': 'Value2',
    'list1': ['JannikWempe', 'svpino', 'MyAspirants'],
    'dict1': {'innerKey1': 'innerValue1', 'innerKey2': 'innerValue2', 'innerKey3': 'innerValue3', }
}

generator = dict_generator_with_indexes(example_dict)
test_generator(generator)
