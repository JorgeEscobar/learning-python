from time import time_ns


def performance(fn):
    def wrapper(*args, **kawrgs):
        t1 = time_ns()
        result = fn(*args, **kawrgs)
        t2 = time_ns()
        t3 = t2 - t1
        one_second = 1_000_000_000
        print(f'{t3} - {one_second}')
        if t3 >= one_second:
            print(f'took {round(t3 / one_second, 3)} seconds.')
        else:
            print(f'took {t3:,} nano seconds.')
        return result
    return wrapper


@performance
def long_time():
    print('1')
    for i in range(100000):
        i*5


@performance
def long_time2():
    print('2')
    for i in list(range(100000)):
        i*5


if __name__ == '__main__':
    long_time()
    long_time2()
