'''2022-05-11 11:33:21'''


from calendar import c


def fib(number):
    previous = 0
    current = 1
    for i in range(number):
        yield previous
        temp = previous
        previous = current
        current += temp

# Execution


for x in fib(21):
    print(x)
