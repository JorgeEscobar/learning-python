'''2022-05-10 17:51:41'''
from locale import currency


class MyGen():
    current = 0

    def __init__(self, first, last) -> None:
        self.first = first
        self.last = last

    def __iter__(self):
        return self

    def __next__(self):
        if MyGen.current < self.last:
            num = MyGen.current
            MyGen.current += 1
            return num
        raise StopIteration


gen = MyGen(1, 100)
for i in gen:
    print(i)
