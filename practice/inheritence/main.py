'''2022-06-03 16:27:32'''
#from super_class import SuperClass
from datetime import date
from sqlite3 import Date
from child_class import ChildClass, SuperClass



# Print the number of SuperClass objets, after instantiating.
# print('No. of objects: ' + str(SuperClass.object_count))
# print(f'{super_object.name}, {super_object.age}')

# today = date.today()
birth_date = date.fromisoformat('1981-05-28')
super_object = SuperClass.from_string('1981-05-28|Jorge Escobar')
another_object = SuperClass(birth_date, 'Jorge Escobar')

super_object.answer_workday = birth_date
print(super_object.answer_workday)