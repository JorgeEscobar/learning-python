'''2022-06-03 16:25:53'''
from super_class import SuperClass

class ChildClass(SuperClass):
    def __init__(self, name='Nameless', age=-1):
        super().__init__(name)
        self.age = age
    
    def suma(self):
        return 'Hola desde ChildClass'