'''2022-06-03 16:12:36'''
'''Practicing inheritence'''
from datetime import date

class SuperClass:
    object_count = 0

    def __init__(self, birth_date:date, name='Nameless'):
        self.name = name
        self.birth_date = birth_date
        SuperClass.addOne()

    @classmethod
    def from_string(cls, obj_str):
        birth_date, name = obj_str.split('|')
        return cls(date.fromisoformat(birth_date), name)
    
    @classmethod
    def addOne(cls):
        cls.object_count += 1

    @staticmethod
    def is_workday(day:date):
        if day.weekday() in [5, 6]:
            return False
        else:
            return True

    @property
    def answer_workday(self):
        day = date.today()
        answer = 'is a workday.' if self.is_workday(day) else 'is not a workday.'
        return f'{day.strftime("%A %-m %B of %Y")} {answer}'

    @answer_workday.setter
    def answer_workday(self, day:date):
        answer = 'is a workday.' if self.is_workday(day) else 'is not a workday.'
        return f'{day.strftime("%A %-m %B of %Y")} {answer}'